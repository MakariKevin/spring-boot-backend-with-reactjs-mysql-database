A simple application using React as frontend and spring boot as backend with MySQL database. 

This specific one is Spring Boot backend (server) – to develop REST API. Check out [Its Frontend Implementation](https://gitlab.com/MakariKevin/spring-boot-frontend-with-reactjs-mysql-database)

## Tools And Technologies Used
- Java 17
- Spring Boot (MVC, JPA, Hibernate)
- MySQL Database
- IntelliJ IDEA

## Output

![Screenshot 2022-11-06 at 03.59.16.png](./Screenshot 2022-11-06 at 03.59.16.png)

![Screenshot 2022-11-06 at 03.59.33.png](./Screenshot 2022-11-06 at 03.59.33.png)

![Screenshot 2022-11-06 at 03.32.57.png](./Screenshot 2022-11-06 at 03.32.57.png)
