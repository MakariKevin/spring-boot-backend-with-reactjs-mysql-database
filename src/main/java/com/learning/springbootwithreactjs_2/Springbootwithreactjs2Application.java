package com.learning.springbootwithreactjs_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbootwithreactjs2Application {

	public static void main(String[] args) {
		SpringApplication.run(Springbootwithreactjs2Application.class, args);
	}

}
